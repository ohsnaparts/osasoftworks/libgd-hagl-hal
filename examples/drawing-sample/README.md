# Drawing Sample

Upon running this sample, a file called [hagl.bmp] is created and subsequently overwritten on every run. This path can be configured via build flags in [platformio.ini].

## Usage

All the necessary steps can be found in

```sh
#!/bin/sh
./run.sh
```

**Have fun! 🥳**

![hagl.bmp][hagl.bmp]


[platformio.ini]: ./drawing-sample/platformio.ini
[hagl.bmp]: ./hagl.bmp