# Samples

To run these samples, you will have to have platformIO fully functional

1. **[Drawing Sample][drawing-sample]**
    > Refer to [./drawing-sample/][drawing-sample] for instructions

   ![](./drawing-sample.gif)

[drawing-sample]: ./drawing-sample/